// $Id: cix.cpp,v 1.4 2016-05-09 16:01:56-07 - - $

/**
 * Edited by Arnaldo Carneiro
 * 1506372
 * ardasilv@ucsc.edu
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
using namespace std;

#include <libgen.h>
#include <sys/types.h>
#include <unistd.h>

#include "protocol.h"
#include "logstream.h"
#include "sockets.h"
#include "util.h"

logstream log (cout);
struct cix_exit: public exception {};

unordered_map<string,cix_command> command_map {
   {"exit", cix_command::EXIT},
   {"help", cix_command::HELP},
   {"ls"  , cix_command::LS  },
   {"get" , cix_command::GET },
   {"rm"  , cix_command::RM  },
   {"put" , cix_command::PUT },
};

void cix_help() {
   static const vector<string> help = {
      "exit         - Exit the program.  Equivalent to EOF.",
      "get filename - Copy remote file to local host.",
      "help         - Print help summary.",
      "ls           - List names of files on remote server.",
      "put filename - Copy local file to remote host.",
      "rm filename  - Remove file from remote server.",
   };
   for (const auto& line: help) cout << line << endl;
}

void cix_ls (client_socket& server) {
   cix_header header;
   header.command = cix_command::LS;
   log << "sending header " << header << endl;
   send_packet (server, &header, sizeof header);
   recv_packet (server, &header, sizeof header);
   log << "received header " << header << endl;
   if (header.command != cix_command::LSOUT) {
      log << "sent LS, server did not return LSOUT" << endl;
      log << "server returned " << header << endl;
   }else {
      char buffer[header.nbytes + 1];
      recv_packet (server, buffer, header.nbytes);
      log << "received " << header.nbytes << " bytes" << endl;
      buffer[header.nbytes] = '\0';
      cout << buffer;
   }
}

void cix_get (client_socket& server, string filename) {
   cix_header header;
   header.command = cix_command::GET;
   strcpy (header.filename, filename.c_str());
   log << "sending header " << header << endl;
   send_packet (server, &header, sizeof header);
   recv_packet (server, &header, sizeof header);
   log << "received header " << header << endl;
   if (header.command != cix_command::FILE) {
      log << "sent get, server did not return FILE" << endl;
      log << "server returned " << header << endl;
   }else {
      ofstream file (header.filename, ios::out|ios::binary|ios::ate);
      if (file.is_open() == false) {
         log << "get " << header.filename <<
                ": file open failed: " << strerror (errno) << endl;
      }else {
         uint32_t received {0};
         while (header.nbytes - received !=0) {
            uint32_t pck_size = min(4096, {header.nbytes - received});
            char buffer[pck_size];
            recv_packet (server, buffer, pck_size);
            received += pck_size;
            file.write (buffer, pck_size);
         }
         file.close();
      }
   }
}

void cix_rm (client_socket& server, string filename) {
   cix_header header;
   header.command = cix_command::RM;
   strcpy (header.filename, filename.c_str());
   log << "sending header " << header << endl;
   send_packet (server, &header, sizeof header);
   recv_packet (server, &header, sizeof header);
   log << "received header " << header << endl;
   if (header.command != cix_command::ACK) {
      log << "sent RM, server did not return ACK" << endl;
      log << "server returned " << header << endl;
   }else {
      log << "sent RM, server returned ACK" << endl;
      log << "server returned " << header << endl;
   }
}

void cix_put (client_socket& server, string filename) {
   ifstream file (filename, ios::in|ios::binary|ios::ate);
   if (file.is_open() == true) {
      cix_header header;
      header.command = cix_command::PUT;
      header.nbytes = file.tellg();
      strcpy (header.filename, filename.c_str());

      log << "sending header " << header << endl;
      send_packet (server, &header, sizeof header);
      recv_packet (server, &header, sizeof header);
      log << "received header " << header << endl;
      
      uint32_t sent {0};
      while (header.nbytes - sent != 0) {
         uint32_t pck_size = min(4096, {header.nbytes - sent});
         char buffer[pck_size];
         file.seekg (sent, ios::beg);
         file.read (buffer, pck_size);
         send_packet (server, buffer, pck_size);
         sent += pck_size;
      }
   }else {
      log << "put " << filename <<
                ": file open failed: " << strerror (errno) << endl;
   }
}

void usage() {
   cerr << "Usage: " << log.execname() << " [host] [port]" << endl;
   throw cix_exit();
}

int main (int argc, char** argv) {
   log.execname (basename (argv[0]));
   log << "starting" << endl;
   vector<string> args (&argv[1], &argv[argc]);
   if (args.size() > 2) usage();
   string host = get_cix_server_host (args, 0);
   in_port_t port = get_cix_server_port (args, 1);
   log << to_string (hostinfo()) << endl;
   try {
      log << "connecting to " << host << " port " << port << endl;
      client_socket server (host, port);
      log << "connected to " << to_string (server) << endl;
      for (;;) {
         string line;
         cout << "c:\\> ";
         getline (cin, line);
         if (cin.eof()) throw cix_exit();
         log << "command " << line << endl;
         vector<string> split_line = split(line, " \t");
         const auto& itor = command_map.find (split_line[0]);
         cix_command cmd = itor == command_map.end()
                         ? cix_command::ERROR : itor->second;
         string line_complement = join(split_line, 1);
         size_t posi;
         switch (cmd) {
            case cix_command::EXIT:
               throw cix_exit();
               break;
            case cix_command::HELP:
               cix_help();
               break;
            case cix_command::LS:
               cix_ls (server);
               break;
            case cix_command::GET:
               cix_get (server, line_complement);
               break;
            case cix_command::RM:
               cix_rm (server, line_complement);
               break;
            case cix_command::PUT:
               cix_put (server, line_complement);
               break;
            default:
               log << line << ": invalid command" << endl;
               break;
         }
      }
   }catch (socket_error& error) {
      log << error.what() << endl;
   }catch (cix_exit& error) {
      log << "caught cix_exit" << endl;
   }
   log << "finishing" << endl;
   return 0;
}


