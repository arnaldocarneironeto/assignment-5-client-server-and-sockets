// $Id: util.cpp,v 1.1 2015-07-16 16:47:51-07 - - $

/**
 * Edited by Arnaldo Carneiro
 * 1506372
 * ardasilv@ucsc.edu
 */

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <sstream>
#include <stdexcept>
#include <string>
#include <typeinfo>
#include <iostream>
#include <fstream>
using namespace std;

#include "util.h"

vector<string> split (const string& line, const string& delimiters) {
   vector<string> words;
   int end = 0;
   for (;;) {
      size_t start = line.find_first_not_of (delimiters, end);
      if (start == string::npos) break;
      end = line.find_first_of (delimiters, start);
      words.push_back (line.substr (start, end - start));
   }
   return words;
}

string join (const vector<string>& split_line, const size_t& begin) {
   string result {split_line[begin]};
   for (size_t i = begin + 1; i < split_line.size(); ++i)
      result += " " + split_line[i];
   return result;
}