// $Id: util.h,v 1.2 2016-05-04 16:26:26-07 - - $

/**
 * Edited by Arnaldo Carneiro
 * 1506372
 * ardasilv@ucsc.edu
 */

//
// util -
//    A utility class to provide various services not conveniently
//    included in other modules.
//

#ifndef __UTIL_H__
#define __UTIL_H__

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
using namespace std;

#include "protocol.h"

//
// split -
//    Split a string into a vector<string>..  Any sequence
//    of chars in the delimiter string is used as a separator.  To
//    Split a pathname, use "/".  To split a shell command, use " ".
//

vector<string> split (const string& line, const string& delimiter);

//
// join -
//    Joins a vector<string> into a string using " " as delimiter..
//

string join (const vector<string>& split_line, const size_t& begin);

#endif

